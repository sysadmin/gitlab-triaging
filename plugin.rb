# frozen_string_literal: true

# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
# SPDX-FileCopyrightText: 2020 Harald Sitter <sitter@kde.org>

# Monkey patch scope=all into merge requests parameters. Otherwise we'll not
# be able to list all MRs, regardless of the query user being in the list.
module Gitlab
  module Triage
    module UrlBuilders
      class UrlBuilder
        alias orig_init initialize
        # NB: This may cause headaches with ruby 3 moving forward should the API
        #   change in triage upstream. options is an ordinary param that is a
        #   hash, this is also a legacy way of writing keyword params though
        #   so if it ever becomes a proper keyword param we need to change the
        #   syntax to **options so we capture it as such.
        def initialize(*args, options)
          if options.fetch(:resource_type) == 'merge_requests'
            options.fetch(:params)[:scope] = 'all'
          end
          orig_init(*args, **options)
        end
      end
    end
  end
end

# Monkey patch project sorting. BasePolicy is the base of all output policies.
module Gitlab
  module Triage
    module Policies
      class BasePolicy
        alias orig_init initialize
        def initialize(*args)
          unless args[2].is_a?(Gitlab::Triage::PoliciesResources::RuleResources)
            raise 'args[2] had unexpected type'
          end

          # Could also sort by project_id but web_url has the advantage of grouping all of plasma etc.
          args[2].sort_by! { |r| r.fetch('web_url') }
          orig_init(*args)
        end
      end
    end
  end
end

module KDEPlugin
  def project_cache
    # This is a bit awkard. The way Context works is that it constructs
    # resources and then extends itself into them. That makes it tricky
    # to use class-instance variables, plus expecting that Context behaves
    # this way is presuming implementation, so instead use a class variable.
    # This means all resources share the same variable though!
    # TODO: look into network.query_api_cached as replacement
    @@project_cache ||= {}
  end

  def targets_group?
    project = project_cache[resource[:project_id]]
    # This is a bit of a hack, we force the resource type to be nil
    # which effectively makes the context url a project url.
    #   https://invent.kde.org/api/v4/projects/943/merge_requests
    #     =>
    #   https://invent.kde.org/api/v4/projects/943
    project_url = build_url(options: { resource_type: nil })
    project ||= network.query_api(project_url).fetch(0)
    project_cache[resource[:project_id]] ||= project
    project.fetch('namespace').fetch('kind') == 'group'
  end

  def wip?
    resource.fetch(:work_in_progress)
  end

  def no_participants?
    url = resource_url(sub_resource_type: 'participants')
    network.query_api(url).size <= 1 # always one participant (the author)
  end

  def kde_member_ids
    return [] if network.options.token.empty? # when we have no token we can't fetch devs... the case during dry runs
    @@kde_member_ids ||= begin
      # Get members of the kde developer group (people with push access).
      # id is hardcoded to save us a query. Also, name is mutable, making this
      # the only reliable way to track the group across possible name changes.
      kde_dev_group_id = '7'
      url = build_url(options: { source: 'groups',
                                 source_id: kde_dev_group_id,
                                 resource_type: 'members'})
      network.query_api(url).collect { |x| x.fetch(:id) }
    end
  end

  def author_is_dev?
    # There appears to be no good and cheap way to determine whether an
    # author has merge access on the project. We'd have to query members for
    # all projects. While that is more accurate it's also super expensive.
    # Instead we'll take a shortcut and simply verify the author is in
    # the kde group. This isn't 100% accurate for all projects but close
    # enough for the purposes of knowing whether a contributor is likely to
    # know what to do.
    kde_member_ids.include?(resource.fetch(:author).fetch(:id))
  end

  # Bit of a crutch. We want to filter MRs that were created more than N weeks
  # ago but we don't want to look at all MRs ever, so we do an API level filter
  # on newer_than M months and then further constrain that via this method.
  def created_at_weeks_ago(weeks_int)
    resource.fetch('created_at').to_date < weeks_int.weeks.ago.to_date
  end
end

Gitlab::Triage::Resource::Context.include(KDEPlugin)
