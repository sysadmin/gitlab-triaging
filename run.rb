#!/usr/bin/env ruby
# frozen_string_literal: true

# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
# SPDX-FileCopyrightText: 2020 Harald Sitter <sitter@kde.org>

require 'optparse'

class Runner
  GITLAB_TRIAGE_CMD = %w[bundle exec ruby
                         -r ./options_patch.rb
                         -S gitlab-triage
                         --require ./plugin.rb
                         --host-url https://invent.kde.org].freeze

  POLICY_FILES = {
    global: '.triage-policies.yml',
    triaging: '.triage-policies-gitlab-triaging.yml'
  }

  def initialize
    @token = ENV.fetch('INVENT_BOT_TOKEN', nil)

    parser = OptionParser.new do |opts|
      opts.banner = "Usage: #{opts.program_name} options"

      policies = POLICY_FILES.keys
      opts.on('--policy [POLICY]', policies, "The policy set to run (#{policies})") do |v|
        @policy = v
      end

      opts.on('--[no-]dry-run', "Don't actually change anything") do |v|
        @dry_run = v
      end

      opts.on('--token [API_TOKEN]',
              'API token; otherwise pulled from $INVENT_BOT_TOKEN') do |v|
        @token = v
      end

      opts.on('--debug', 'Enables debug') do |v|
        @debug = v
      end
    end
    parser.parse!

    # policy is required
    unless @policy
      warn "Policy missing!\n"
      abort parser.help
    end

    return unless @dry_run.nil?

    warn "Dry run must either be explicit on or explicit off!\n"
    abort parser.help
  end

  def cmdline
    cmd = GITLAB_TRIAGE_CMD.dup
    cmd << '--token' << @token if @token
    cmd << '--dry-run' if @dry_run
    cmd << '--debug' if @debug
    cmd << '--policies-file' << POLICY_FILES.fetch(@policy)
    case @policy
    when :global
      cmd << '--all-projects'
    when :triaging
      cmd << '--source-id' << 'teams/gardening/gitlab'
    else
      raise "Unknown policy #{@policy}"
    end
  end

  def run
    cmd = cmdline
    if @debug
      p self
      puts "Running #{cmd}"
    end
    system(*cmd)
  end
end

exit(Runner.new.run ? 0 : 1) # run returns the bool from the system()
