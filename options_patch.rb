# frozen_string_literal: true

# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
# SPDX-FileCopyrightText: 2020 Harald Sitter <sitter@kde.org>

require 'gitlab/triage/options'

# Monkey patch hack to fix all-projects not working because source defaults to
# :projects which is later asserted cannot be set at the same time as all.
module Gitlab
  module Triage
    class Options
      def all=(value)
        self[:source] = nil if value
        self[:all] = value
      end
    end
  end
end
